#include "animation.h"

namespace anisec {

Animation::Animation()
{
    m_idx = 0;
    m_dt = 0;
}

Animation::~Animation()
{

}

void Animation::setFrame(int idx)
{
	sf::Vector2u sz = m_frames[idx].getSize();
    m_sprite.setTexture(m_frames[idx]);
	m_sprite.setOrigin(sz.x / 2, sz.y / 2);
    m_idx = idx;
}

void Animation::addFrame(const std::string &file)
{
	if (!fileExists(file))
		return;

    sf::Texture tex;
    if(!tex.loadFromFile(file))
        return;

    m_frames.push_back(tex);
}

void Animation::nextFrame()
{
    ++m_idx;
    if(m_idx > m_frames.size() - 1)
        m_idx = 0;
    setFrame(m_idx);
}

void Animation::update(float dt)
{
    m_dt += dt;
    if(m_dt > 0.02f)
    {
        m_dt = 0;
        nextFrame();
    }
}

}
