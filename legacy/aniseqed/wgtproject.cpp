#include "wgtproject.h"
#include "ui_wgtproject.h"

WgtProject::WgtProject(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WgtProject)
{
    ui->setupUi(this);
}

WgtProject::~WgtProject()
{
    delete ui;
}


void WgtProject::setSceneData(SceneData *data)
{
    m_data = data;
    ui->txtName->setText(QString(data->getName().c_str()));
}

void WgtProject::apply()
{
    string name = ui->txtName->text().toStdString();
    m_data->setName(name);
}
