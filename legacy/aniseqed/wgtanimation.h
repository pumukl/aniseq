#ifndef WGTANIMATION_H
#define WGTANIMATION_H

#include <QWidget>
#include <data.h>
using namespace aniseq;

namespace Ui {
class WgtAnimation;
}

class WgtAnimation : public QWidget
{
    Q_OBJECT

public:
    explicit WgtAnimation(QWidget *parent = 0);
    ~WgtAnimation();

    void setAnimation(AnimationData* data);
    void apply();


private slots:
    void on_btnApply_clicked();

private:
    AnimationData* m_data;
    Ui::WgtAnimation *ui;
};

#endif // WGTANIMATION_H
