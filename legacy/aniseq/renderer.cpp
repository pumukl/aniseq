#include "renderer.h"

namespace aniseq {

Renderer::Renderer()
{

}

Renderer::~Renderer()
{

}

int Renderer::run(SceneData& scene,
                  AnimationDataList& animations)
{
    int result = 0;


    int flags = 0;
    sf::VideoMode video;
    if(scene.getFullscreen())
    {
        video = sf::VideoMode::getDesktopMode();
        flags = sf::Style::Close | sf::Style::Titlebar;
    }
    else
    {
        video = sf::VideoMode(scene.getResolution().x,scene.getResolution().y);
        flags = sf::Style::Fullscreen;
    }


    sf::RenderWindow window(video, "aniseq", flags);
    window.setFramerateLimit(60);
    sf::View view = window.getView();
    view.setCenter(0, 0);
    window.setView(view);
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed)
                if (event.key.code = sf::Keyboard::Escape)
                    window.close();
        }


        window.clear(scene.getColor());
        // draw to render target
        window.display();

    }

    return result;
}

}
