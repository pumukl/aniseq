cmake_minimum_required(VERSION 2.8) 
project(aniseqed) 

find_package(Qt5Gui REQUIRED)
find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)


set(CMAKE_AUTOMOC on) 

set(src main.cpp
	mainwindow.ui
	mainwindow.cpp
	wgtanimation.ui
	wgtanimation.cpp
	wgtproject.ui
	wgtproject.cpp
	)


include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(../aniseq) 
include_directories(../external/SFML/include)
include_directories(../external/RtMidi)
include_directories(${LCFG_DIR})
include_directories(${Qt5Gui_INCLUDE_DIRS})
include_directories(${Qt5Core_INCLUDE_DIRS})
include_directories(${Qt5Widgets_INCLUDE_DIRS}) 


add_executable(aniseqed ${src})
target_link_libraries(aniseqed config aniseq sfml-system sfml-graphics rtmidi)


qt5_use_modules(aniseqed Core Gui Widgets)
qt5_wrap_ui(ui_mainwindow.h mainwindow.ui) 
qt5_wrap_ui(ui_wgtanimation.h wgtanimation.ui) 
qt5_wrap_ui(ui_wgtproject.h wgtproject.ui) 
