#include "wgtanimation.h"
#include "ui_wgtanimation.h"
#include <QString>

WgtAnimation::WgtAnimation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WgtAnimation)
{
    ui->setupUi(this);
}

WgtAnimation::~WgtAnimation()
{
    delete ui;
}

void WgtAnimation::setAnimation(AnimationData *data)
{
    m_data = data;
    ui->txtName->setText(data->getName().c_str());
    ui->sbKey->setValue(data->getKey());
    ui->sbFrameTime->setValue(data->getFrameTime());
    ui->sbRotation->setValue(data->getRotation());
    ui->sbPosX->setValue(data->getPosition().x);
    ui->sbPosY->setValue(data->getPosition().y);
    ui->sbOffsetX->setValue(data->getOffset().x);
    ui->sbOffsetY->setValue(data->getOffset().y);
}

void WgtAnimation::apply()
{
    m_data->setName(ui->txtName->text().toStdString());
    m_data->setKey((byte)ui->sbKey->value());
    m_data->setRotation((float)ui->sbRotation->value());
    int x = ui->sbPosX->value();
    int y = ui->sbPosY->value();
    int ox = ui->sbOffsetX->value();
    int oy = ui->sbOffsetY->value();
    m_data->setPosition(sf::Vector2i(x, y));
    m_data->setOffset(sf::Vector2i(ox, oy));
    m_data->setFrameTime(ui->sbFrameTime->value());
}

void WgtAnimation::on_btnApply_clicked()
{
    apply();
}
