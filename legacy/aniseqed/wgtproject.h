#ifndef WGTPROJECT_H
#define WGTPROJECT_H

#include <QWidget>

namespace Ui {
class WgtProject;
}

#include <data.h>
using namespace aniseq;

class WgtProject : public QWidget
{
    Q_OBJECT

public:
    explicit WgtProject(QWidget *parent = 0);
    ~WgtProject();

    void setSceneData(SceneData* data);
    void apply();

private:
    SceneData* m_data;
    Ui::WgtProject *ui;
};

#endif // WGTPROJECT_H
