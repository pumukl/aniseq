#pragma once
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>


namespace aniseq {

class Animation
{
public:
    Animation();
    ~Animation();

    void nextFrame();
    void setFrame(int idx);
    void addFrame(const std::string& file);
    sf::Sprite getSprite() { return m_sprite; }

	inline int getFrameCount() { return m_frames.size(); }

    void update(float dt);

private:
    int m_idx;
    float m_dt;
    sf::Sprite m_sprite;
    std::vector<sf::Texture> m_frames;
};

}


