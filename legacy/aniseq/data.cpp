#include "data.h"

namespace aniseq {

#define cfg_readInt(N, V, C, E) \
    try { \
    V = (int)C.lookup(N); \
    } catch(SettingNotFoundException& nfex) { \
    E = N; \
    }

#define cfg_readFlt(N, V, C, E) \
    try { \
    V = (float)C.lookup(N); \
    } catch(SettingNotFoundException& nfex) { \
    E = N; \
    }


#define cfg_readStr(N, V, C, E) \
    try { \
    V = (const char*)setting.lookup(N); \
    } catch(SettingNotFoundException& nfex) {\
    E = N; \
    }


bool Data::load(libconfig::Setting &setting, string& error)
{
    return false;
}

bool Data::save(libconfig::Setting &setting, string& error)
{
    return false;
}

bool ModifierData::save(libconfig::Setting& setting, string& error)
{
    return false;
}

bool ModifierData::load(libconfig::Setting& setting, string& error)
{
    return false;
}

AnimationData::AnimationData()
{

}

AnimationData::~AnimationData()
{
    list<ModifierData*>::iterator it;
    for(it = m_modifiers.begin(); it != m_modifiers.end(); ++it)
    {
        delete (*it);
    }
}

bool AnimationData::save(libconfig::Setting& setting, string& error)
{
    bool result = true;
    error = "";
    {
        using namespace libconfig;
        try
        {
            Setting &config = setting.add(m_name.c_str(), Setting::TypeGroup);
            config.add("name", Setting::TypeString) = m_name.c_str();
            config.add("key", Setting::TypeInt) = m_key;
            config.add("frameTime", Setting::TypeInt) = m_frameTime;
            config.add("rotation", Setting::TypeFloat) = m_rotation;
            config.add("positionX", Setting::TypeInt) = m_position.x;
            config.add("positionY", Setting::TypeInt) = m_position.y;
            config.add("offsetX", Setting::TypeInt) = m_offset.x;
            config.add("offsetY", Setting::TypeInt) = m_offset.y;
            config.add("colorA", Setting::TypeInt) = m_color.a;
            config.add("colorR", Setting::TypeInt) = m_color.r;
            config.add("colorG", Setting::TypeInt) = m_color.g;
            config.add("colorB", Setting::TypeInt) = m_color.b;
            Setting& mods = config.add("modifiers", Setting::TypeList);
            list<ModifierData*>::iterator it = m_modifiers.begin();
            for(;it != m_modifiers.end(); ++it)
            {
                (*it)->save(mods, error);
            }
        }
        catch(std::exception& ex)
        {
            error = ex.what();
            return false;
        }
    }

    return result;
}

bool AnimationData::load(libconfig::Setting& setting, string& error)
{
    bool result = true;
    error = "";
    {
        using namespace libconfig;
        cfg_readStr("name", m_name, setting, error);
        cfg_readInt("key", m_key, setting, error);
        cfg_readInt("frameTime", m_frameTime, setting, error);
        cfg_readFlt("rotation", m_rotation, setting, error);
        cfg_readInt("positionX", m_position.x, setting, error);
        cfg_readInt("positionY", m_position.y, setting, error);
        cfg_readInt("offsetX", m_offset.x, setting, error);
        cfg_readInt("offsetY", m_offset.y, setting, error);
        cfg_readInt("colorA", m_color.a, setting, error);
        cfg_readInt("colorR", m_color.r, setting, error);
        cfg_readInt("colorG", m_color.g, setting, error);
        cfg_readInt("colorB", m_color.b, setting, error);
        try
        {
            Setting& mods = setting.lookup("modifiers");
            if(mods.getType() != Setting::TypeList)
            {
                error = "Modifiers are not a list!";
                return false;
            }
            for(int i = 0; i < mods.getLength(); ++i)
            {
                if(mods[i].getType() == Setting::TypeGroup)
                {
                    ModifierData* data = new ModifierData();
                    if(!data->load(mods[i], error))
                        return false;
                }
            }

        }
        catch(SettingNotFoundException nfex)
        {
            error = "No modifiers found!";
        }
    }
    return result;
}


SceneData::SceneData()
{
    m_name = "untitled";
    m_clearColor = sf::Color::White;
    m_fullScreen = false;
    m_resolution.x = 1280;
    m_resolution.y = 720;
}

SceneData::~SceneData()
{

}

bool SceneData::save(libconfig::Setting& setting, string& error)
{
    bool result = true;
    error = "";
    {
        using namespace libconfig;
        try
        {
            Setting &config = setting.add("scene", Setting::TypeGroup);
            config.add("name", Setting::TypeString) = m_name.c_str();
            config.add("resolutionX", Setting::TypeInt) = m_resolution.x;
            config.add("resolutionY", Setting::TypeInt) = m_resolution.y;
            config.add("fullscreen", Setting::TypeInt) = (int)m_fullScreen;
            config.add("colorA", Setting::TypeInt) = m_clearColor.a;
            config.add("colorR", Setting::TypeInt) = m_clearColor.r;
            config.add("colorG", Setting::TypeInt) = m_clearColor.g;
            config.add("colorB", Setting::TypeInt) = m_clearColor.b;
            config.add("source", Setting::TypeInt) = (int)m_source;
        }
        catch(std::exception& ex)
        {
            error = ex.what();
        }
    }

    return result;
}

bool SceneData::load(libconfig::Setting& setting, string& error)
{
    bool result = true;
    error = "";
    {
        using namespace libconfig;
        cfg_readStr("name", m_name, setting, error);
        cfg_readInt("source", m_source, setting, error);
        cfg_readInt("fullscreen", m_fullScreen, setting, error);
        cfg_readInt("resolutionX", m_resolution.x, setting, error);
        cfg_readInt("resolutionY", m_resolution.y, setting, error);
        cfg_readInt("colorA", m_clearColor.a, setting, error);
        cfg_readInt("colorR", m_clearColor.r, setting, error);
        cfg_readInt("colorG", m_clearColor.g, setting, error);
        cfg_readInt("colorB", m_clearColor.b, setting, error);
    }
    return result;
}


bool loadConfig(SceneData* scene, list<AnimationData*>& anims, const string& file, string& error)
{
    bool result = true;
    error = "";
    {
        using namespace libconfig;
        Config cfg;
        try
        {
            cfg.readFile(file.c_str());
        }
        catch(FileIOException &fioex)
        {
            error = "File IO Exception!";
            return false;
        }

        try
        {
            Setting& r = cfg.getRoot();

            if(!r.exists("scene"))
            {
                error = "No scene found in root!";
                return false;
            }
            Setting& s = r["scene"];
            if(!scene->load(s, error))
            {
                error = "Could not load scene!";
                return false;
            }

            if(!r.exists("animations"))
            {
                error = "No animations found in root!";
                return false;
            }
            Setting& a = r["animations"];
            for(int i = 0; i < a.getLength(); ++i)
            {
                AnimationData* data = new AnimationData();
                if(!data->load(a[i], error))
                    delete data;
                else
                    anims.push_back(data);
            }
        }
        catch(...)
        {
            error = "Error reading data!";
            return false;
        }

    }
    return result;
}

bool saveConfig(SceneData* data, list<AnimationData*>& anims, const string& file, string& error)
{
    bool result = true;
    error = "";
    {
        using namespace libconfig;
        Config cfg;
        try
        {
            Setting& r = cfg.getRoot();

            if(!data->save(r, error))
            {
                error = "Could not write scene data!";
                return false;
            }

            r.add("animations", Setting::TypeGroup);
            Setting& a = r["animations"];
            list<AnimationData*>::iterator it;
            for(it = anims.begin(); it != anims.end(); ++it)
            {
                if(!(*it)->save(a, error))
                    return false;
            }

            cfg.writeFile(file.c_str());
        }
        catch(...)
        {
            error = "Error writing config!";
            return false;
        }
    }
    return result;
}

}

