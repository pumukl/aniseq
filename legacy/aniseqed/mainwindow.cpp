#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWidget>
#include <QRect>
#include <QSplitter>
#include <QWindow>
#include <QMenuBar>
#include <QLayout>
#include <QVariant>
#include <QResizeEvent>
#include <QInputDialog>
#include <QModelIndex>
#include <QModelIndexList>
#include <QItemSelectionRange>
#include <QMainWindow>
#include <QDir>
#include <QDirModel>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QFileDialog>
#include <QMessageBox>
#include <QErrorMessage>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    m_menuFile = NULL;
    m_wgtProject = NULL;
    m_wgtObjects = NULL;
    m_wgtAnimation = NULL;
    ui->setupUi(this);
    createActions();
    createMenus(false);
    createViews();
}

MainWindow::~MainWindow()
{
    delete ui;
    if(m_menuFile)
        delete m_menuFile;

    if(m_wgtObjects)
        delete m_wgtObjects;

    if(m_wgtAnimation)
        delete m_wgtAnimation;

    if(m_wgtProject)
        delete m_wgtProject;
}


void MainWindow::createActions()
{
    m_actNew = new QAction(tr("New project"), this);
    connect(m_actNew, SIGNAL(triggered()), this, SLOT(newProject()));
    m_actLoad = new QAction(tr("Load project"), this);
    connect(m_actLoad, SIGNAL(triggered()), this, SLOT(loadProject()));
    m_actSave = new QAction(tr("Save project"), this);
    connect(m_actSave, SIGNAL(triggered()), this, SLOT(saveProject()));
    m_actSaveAs = new QAction(tr("Save project as"), this);
    connect(m_actSaveAs, SIGNAL(triggered()), this, SLOT(saveProjectAs()));
    m_actClose = new QAction(tr("Close project"), this);
    connect(m_actClose, SIGNAL(triggered()), this, SLOT(closeProject()));
    m_actExit = new QAction(tr("Exit"), this);
    connect(m_actExit, SIGNAL(triggered()), this, SLOT(exitApp()));

    m_actAniAdd = new QAction(tr("Add Animation"), this);
    connect(m_actAniAdd, SIGNAL(triggered()), this, SLOT(addAnimation()));
    m_actAniDel = new QAction(tr("Delete Animation"), this);
    connect(m_actAniDel, SIGNAL(triggered()), this, SLOT(delAnimation()));

}

void MainWindow::createMenus(bool projectActive)
{
    menuBar()->clear();

    if(m_menuFile)
    {
        m_menuFile->clear();
        delete m_menuFile;
    }
    m_menuFile = menuBar()->addMenu(tr("Project"));

    if(projectActive)
    {
        m_menuFile->addAction(m_actNew);
        m_menuFile->addAction(m_actLoad);
        m_menuFile->addAction(m_actSave);
        m_menuFile->addAction(m_actSaveAs);
        m_menuFile->addAction(m_actClose);
        m_menuFile->addAction(m_actExit);

    }
    else
    {
        m_menuFile->addAction(m_actNew);
        m_menuFile->addAction(m_actLoad);
        m_menuFile->addAction(m_actExit);
    }

}

void MainWindow::createViews()
{
    if(!m_wgtAnimation)
        m_wgtAnimation = new WgtAnimation(this);

    if(!m_wgtObjects)
        m_wgtObjects = new QTreeView(this);


    m_trModel = new QStandardItemModel();
    QStandardItem* root = m_trModel->invisibleRootItem();

    ObjectData* od = new ObjectData();
    od->type = ObjectType::Project;
    od->data = &m_sceneData;
    QVariant data = QVariant::fromValue((void*)od);
    m_trnProject = new QStandardItem("Project");
    m_trnProject->setData(data);
    root->appendRow(m_trnProject);

    od = new ObjectData();
    od->type = ObjectType::Project;
    od->data = &m_sceneData;
    data = QVariant::fromValue((void*)od);
    m_trnAnim = new QStandardItem("Animations");
    m_trnAnim->setData(data);
    root->appendRow(m_trnAnim);

    m_wgtObjects->setModel(m_trModel);
    m_wgtObjects->expandAll();

    m_isModel = m_wgtObjects->selectionModel();
    connect(m_isModel, SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
            this, SLOT(objSelect(QItemSelection,QItemSelection)));

    if(!m_wgtProject)
        m_wgtProject = new WgtProject(this);

    connect(this, SIGNAL(resizeEvent(QResizeEvent*)), this, SLOT(resizeEvent(QResizeEvent*)));

    m_wgtObjects->hide();
    m_wgtProject->hide();
    m_wgtAnimation->hide();
}

void MainWindow::clearTree()
{
    if(m_trnAnim->hasChildren())
    {
        for(int i = 0; i < m_trnAnim->rowCount(); ++i)
        {
            QVariant v = m_trnAnim->child(i)->data();
            void* odp = v.value<void*>();
            delete (ObjectData*)odp;
        }
        m_trnAnim->removeRows(0, m_trnAnim->rowCount());
    }
}


void MainWindow::switchView(ViewMode mode)
{
    m_wgtObjects->hide();
    m_wgtProject->hide();
    m_wgtAnimation->hide();
    switch(mode)
    {
    case ViewMode::Nothing:
        break;
    case ViewMode::Project:
        m_wgtObjects->show();
        m_wgtProject->show();
        break;
    case ViewMode::Animation:
        m_wgtObjects->show();
        m_wgtAnimation->show();
        break;
    case ViewMode::Modifier:
        break;
    }
}

void MainWindow::addAnimation()
{

}

void MainWindow::delAnimation()
{

}

void MainWindow::newProject()
{
    bool ok = false;
    QString name = QInputDialog::getText(this, tr("New project"), tr("Name:"), QLineEdit::Normal, "", &ok);
    name = name.replace(" ","_");

    if(!ok)
        return;

    // QString name = QFileDialog::getExistingDirectory(this, tr("Project folder"), QDir::homePath());
    QString folder = QFileDialog::getExistingDirectory(this, tr("Project folder"), "/home/mau/Workspace/aniseq/deliver");
    if(folder == "")
        return;

    QString cfg = QString("%1/%2.%3").arg(folder, name, ANISEQ_SUFFIX);
    m_cfgFile = cfg.toStdString();

    QDirIterator it(folder);
    while (it.hasNext())
    {
        it.next();
        if(     QFileInfo(it.filePath()).isFile() &&
                it.filePath() == cfg)
        {
            QMessageBox mb(this);
            mb.setText(tr("Project already exists!"));
            mb.setInformativeText(tr("Load existing project?"));
            mb.setStandardButtons(QMessageBox::Open | QMessageBox::Cancel);
            int ret = mb.exec();
            switch(ret)
            {
            case QMessageBox::Open:
                loadProject(it.filePath().toStdString());
                return;
            case QMessageBox::Cancel:
                return;
            }
        }

    }

    m_sceneData.setName(name.toStdString());

    string err;
    if(!saveConfig(&m_sceneData, m_animations, cfg.toStdString(), err))
    {
        return;
    }

//    AnimationData* data = new AnimationData();
//    data->setName("foobar");
//    data->setRotation(45);
//    m_animations.push_back(data);

    loadedProject();
}

void MainWindow::loadProject()
{
    QString str = QString("%1 (*.%2)").arg(ANISEQ_SUFFIX, ANISEQ_SUFFIX);
    QString file = QFileDialog::getOpenFileName(this, tr("Load project file"), "/home/mau/Workspace/aniseq/deliver", str);
    if(file == "")
        return;

    loadProject(file.toStdString());
}

void MainWindow::saveProject()
{
    if(m_cfgFile == "")
        return;

    string err;
    if(!saveConfig(&m_sceneData, m_animations, m_cfgFile, err))
    {
        //errorMessage(tr("Error saving project!"), err.c_str());
    }
}

void MainWindow::saveProjectAs()
{

}

void MainWindow::closeProject()
{

}

void MainWindow::exitApp()
{

}

void MainWindow::objsUpdated()
{
    clearTree();
    list<AnimationData*>::iterator it;
    for(it = m_animations.begin(); it != m_animations.end(); ++it)
    {
        ObjectData* od = new ObjectData();
        od->type = ObjectType::Animation;
        od->data = (*it);
        string name = (*it)->getName();
        QVariant data = QVariant::fromValue((void*)od);
        QStandardItem* item = new QStandardItem(name.c_str());
        item->setData(data);
        m_trnAnim->appendRow(item);
    }
}

void MainWindow::objSelect(QItemSelection selected, QItemSelection deselected)
{
    QModelIndexList lst = selected.indexes();
    QModelIndex idx = (*lst.begin());
    QVariant v = idx.data(Qt::UserRole + 1);
    void* odp = v.value<void*>();
    ObjectData* od = (ObjectData*)odp;
    if(od->type == ObjectType::Animation)
    {
        m_wgtAnimation->setAnimation((AnimationData*)od->data);
        switchView(ViewMode::Animation);
    }
    else if(od->type == ObjectType::Project)
    {
        m_wgtProject->setSceneData((SceneData*)od->data);
        switchView(ViewMode::Project);
    }
}

void MainWindow::resizeEvent(QResizeEvent *evt)
{
    int width = 256;
    QRect r = contentsRect();
    QRect mb = menuBar()->geometry();
    r.setWidth(width);
    r.setTop(mb.height());
    m_wgtObjects->setGeometry(r);
    r = contentsRect();
    r.setLeft(width);
    r.setTop(mb.height());
    m_wgtProject->setGeometry(r);
    m_wgtAnimation->setGeometry(r);
}

void MainWindow::loadProject(const string &file)
{
    m_cfgFile = file;
    string error;
    if(!loadConfig(&m_sceneData, m_animations, file, error))
    {

    }
    else
    {
        loadedProject();
    }
}

void MainWindow::loadedProject()
{
    createMenus(true);
    objsUpdated();
    switchView(ViewMode::Project);
    setWindowTitle(m_sceneData.getName().c_str());
}
