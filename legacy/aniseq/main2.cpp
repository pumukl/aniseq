#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <map>
using namespace std;
#include "RtMidi.h"
#include "config.h"
#include "animation.h"


static bool stepPending = false;
static unsigned char currTrack = -1;
static unsigned char nextTrack = 36;

void readCallback(double deltatime, std::vector<unsigned char> *message, void *userData)
{
	//cout << "[" << message->at(0) << "]" << endl;
	if (message->at(0) == 0xf8)
	{
		stepPending = true;
	}
	if (message->at(0) == 0x90)
	{
		if (currTrack != message->at(1))
		{
			nextTrack = message->at(1);
		}
	}
}

void tryLoadFrame(Animation* anim, int id, const string& name, const std::string& ext)
{
	stringstream ss;
	ss << "gfx/" << name;
	ss << "/" << id;
	ss << "." << ext;
	anim->addFrame(ss.str());
}

int main(int argc, char* argv[])
{
	int midiPort = 0;
	AniMap anims;
	readAnimations("aniseq.cfg", anims);

	cout << "Initializing midi";
	RtMidiIn* midi;
	try
	{
		midi = new RtMidiIn();
		midi->ignoreTypes(true, false, true);
		midi->setCallback(readCallback);
	}
	catch (RtMidiError& error)
	{
		cout << "Error with midi! " << error.getMessage() << endl;
		return 0;
	}
	cout << " ok!" << endl;

	cout << "Midi devices:" << endl;
	int n = midi->getPortCount();
	for (int i = 0; i < n; ++i)
	{
		try
		{
			string name(midi->getPortName(i).c_str());
			cout << i << ": " << name << endl;
		}
		catch (RtMidiError& error)
		{
			cout << error.getMessage() << endl;
		}
	}
	cout << "----------------------------------------" << endl;
	cout << "Please enter the number to use ..." << endl;

	cin >> midiPort;

	try
	{
		midi->openPort(midiPort);
	}
	catch (RtMidiError& error)
	{
		cout << "Error with midi! " << error.getMessage() << endl;
		return 0;
	}


	map<unsigned char, Animation*> animations;
	AniMap::const_iterator it = anims.begin();
	for (; it != anims.end(); ++it)
	{
		AnimationData ad = (*it).second;
		Animation* anim = new Animation();
		cout << "Loading animation " << (*it).first << " ( " << (int)ad.id << " )";
		if (ad.frames.size())
		{
			for (int i = 0; i < ad.frames.size(); ++i)
				anim->addFrame(ad.frames[i]);			
		}
		else
		{
			// if there are no frames defined we will look for up to 32 frames
			// of the types bmp, jpeg, png 
			// the file ending may vary but the ids must be UNIQE !!!
			for (int i = 0; i < 32; ++i)
			{
				tryLoadFrame(anim, i, ad.name, "png");
				tryLoadFrame(anim, i, ad.name, "bmp");
				tryLoadFrame(anim, i, ad.name, "jpeg");
			}
		}
		cout << " with " << anim->getFrameCount() << " frames" << endl;
		anim->setFrame(0);
		animations.insert({ ad.id, anim });
	}

	int startAni = -1;
	while (true)
	{
		cout << "----------------------------------------" << endl;
		cout << "Please enter start animation ( see above ) ..." << endl;
		cin >> startAni;
		if (animations.find(startAni) != animations.end())
		{
			nextTrack = startAni;
			break;
		}
	}
	

	sf::VideoMode vm(1280, 720);
	int style = sf::Style::Titlebar | sf::Style::Close;
	string fullscreen;
	cout << "----------------------------------------" << endl;
	cout << "Fullscreen ? y/n or default no " << endl;
	cin >> fullscreen;
	if (fullscreen == "y" || fullscreen == "yes")
	{
		style = sf::Style::Fullscreen;
		vm = sf::VideoMode::getDesktopMode();
	}
	



	Animation* currAnim = NULL;
	sf::RenderWindow window(vm, "AniSeq dedicated renderer", style);
	window.setFramerateLimit(60);
	sf::View view = window.getView();
	view.setCenter(0, 0);
	window.setView(view);
	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::KeyPressed)
				if (event.key.code = sf::Keyboard::Escape)
					window.close();
		}

		if (currTrack != nextTrack)
		{
			if (animations.find(nextTrack) != animations.end())
			{
				currTrack = nextTrack;
				currAnim = animations[currTrack];
			}
			else
			{
				cout << "Track: " << (int)nextTrack << " not registerd!" << endl;
				nextTrack = currTrack;
			}
		}

		if (stepPending)
		{
			currAnim->nextFrame();
			stepPending = false;
		}

		window.clear();
		window.draw(currAnim->getSprite());
		window.display();

	}
	midi->closePort();
	return 0;
}
