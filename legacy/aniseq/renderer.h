#pragma once

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "data.h"

#include <map>
#include <list>
using namespace std;

namespace aniseq {


///////////////////////////////////////////////////////////////////////////////
/// http://www.music-software-development.com/midi-tutorial.html
/// http://www.midi.org/techspecs/midimessages.php
///////////////////////////////////////////////////////////////////////////////

enum class RendererSource : int
{
    RtMidi = 0,
    File
};

class Animation;
class Modifier;
class Renderer
{
public:
    int run(SceneData& scene,
            AnimationDataList& animations);

public:
    void update(const sf::Time& delta, byte status, byte data0, byte data1);
    void render(const sf::RenderTarget& target);

private:
    map<byte,Animation*> m_animations;
    RendererSource m_source;


public:
    Renderer();
    ~Renderer();
};

}
