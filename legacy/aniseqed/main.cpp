#include "mainwindow.h"
#include <QApplication>

#include <libconfig.h++>
#include <data.h>


void data_test();


int main(int argc, char *argv[])
{

    data_test();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

void data_test()
{
    using namespace aniseq;
    using namespace libconfig;
    list<AnimationData*> anims;
    AnimationData* data0 = new AnimationData();
    data0->setName("foobar");
    data0->setFrameTime(800);
    ModifierData* md0 = new ModifierData();
    md0->setName("mod0");
    data0->addModifier(md0);
    md0 = new ModifierData();
    md0->setName("mod1");
    data0->addModifier(md0);

    anims.push_back(data0);


    data0 = new AnimationData();
    data0->setName("barfoo");
    data0->setFrameTime(800);
    md0 = new ModifierData();
    md0->setName("mod2");
    data0->addModifier(md0);
    md0 = new ModifierData();
    md0->setName("mod3");
    data0->addModifier(md0);

    anims.push_back(data0);


    SceneData scene;

    string error;
    saveConfig(&scene, anims, "test.cfg", error);
    anims.clear();
    loadConfig(&scene, anims, "test.cfg", error);


    try
    {
        Config cfg;
        string error = "All good ...";
    }
    catch(...)
    {
        return;
    }

    AnimationData* verify = new AnimationData();

}
