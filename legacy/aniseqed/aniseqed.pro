#-------------------------------------------------
#
# Project created by QtCreator 2015-05-02T14:50:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = aniseqde
TEMPLATE = app

FORMS += \
    mainwindow.ui

HEADERS += \
    mainwindow.h \
    ../anisec/data.h

SOURCES += \
    mainwindow.cpp \
    main.cpp \
    ../anisec/data.cpp

win32:LIBS += ""
linux:LIBS += -lconfig++
