#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <list>
#include <string>
using namespace std;

#include <data.h>
using namespace aniseq;

#include <QMenu>
#include <QAction>
#include <QMainWindow>
#include <QTreeView>
#include <QItemSelectionModel>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QListWidget>

#include "wgtproject.h"
#include "wgtanimation.h"


namespace Ui {
class MainWindow;
}

enum class ViewMode
{
    Nothing,
    Project,
    Animation,
    Modifier
};

enum class Views : int
{
    Objects = 0,
    Project,
    Animation,
    Modifier
};

enum class ObjectType : int
{
    None = 0,
    Project,
    Animation,
    Modifier,
    Trigger
};

struct ObjectData
{
    ObjectType type;
    void* data;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void newProject();
    void loadProject();
    void saveProject();
    void saveProjectAs();
    void closeProject();
    void exitApp();

    void addAnimation();
    void delAnimation();

//    void addModifier();
//    void delModifier();

    void objsUpdated();
    void objSelect(QItemSelection s,QItemSelection d);    void resizeEvent(QResizeEvent* evt);



private:
    QMenu* m_menuFile;
    QAction* m_actNew;
    QAction* m_actLoad;
    QAction* m_actSave;
    QAction* m_actSaveAs;
    QAction* m_actClose;
    QAction* m_actExit;

    QAction* m_actAniAdd;
    QAction* m_actAniDel;
    QAction* m_actModAdd;
    QAction* m_actModDel;

    QTreeView* m_wgtObjects;
    QStandardItem* m_trnAnim;
    QStandardItem* m_trnProject;
    QStandardItemModel* m_trModel;
    QItemSelectionModel* m_isModel;

    WgtProject* m_wgtProject;
    WgtAnimation* m_wgtAnimation;

private:
    void createViews();
    void createActions();
    void createMenus(bool projectActive);
    void loadProject(const string& file);
    void loadedProject();

    void switchView(ViewMode mode);

    void clearTree();


    void errorMessage(const QString& title, const QString& text);


private:
    string m_cfgFile;
    SceneData m_sceneData;
    list<AnimationData*> m_animations;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
