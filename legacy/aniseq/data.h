#pragma once
#include <string>
#include <map>
#include <list>
using namespace std;
#include <libconfig.h++>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

namespace aniseq {

#define ANISEQ_SUFFIX "aniseq"

typedef unsigned char byte;
typedef unsigned int uint;

class Data
{
public:
    virtual bool save(libconfig::Setting& setting, string& error);
    virtual bool load(libconfig::Setting& setting, string& error);
};

class ModifierData : public Data
{
private:
    friend class Modifier;
    byte type;
    string m_name;

public:
    virtual bool save(libconfig::Setting& setting, string& error);
    virtual bool load(libconfig::Setting& setting, string& error);

public:
    void setName(const string& name) { m_name = name; }
    const string& getName() { return m_name; }

    ModifierData(){}
    ~ModifierData(){}
};



class SceneData : public Data
{
private:
    string m_name;
    int m_source;
    bool m_fullScreen;
    sf::Color m_clearColor;
    sf::Vector2i m_resolution;

public:
    void setName(const string& name) { m_name = name; }
    const string& getName() { return m_name; }

    void setColor(sf::Color& color) { m_clearColor = color; }
    sf::Color getColor() { return m_clearColor; }

    void setSource(int source) { m_source = source; }
    int getSource() { return m_source; }

    void setResolution(const sf::Vector2i& v) { m_resolution = v; }
    sf::Vector2i getResolution() { return m_resolution; }

    void setFullscreen(bool fs) { m_fullScreen = fs; }
    bool getFullscreen() { return m_fullScreen; }

public:
    virtual bool save(libconfig::Setting& setting, string& error);
    virtual bool load(libconfig::Setting& setting, string& error);

    SceneData();
    ~SceneData();
};

class AnimationData : public Data
{
private:
    friend class Animation;
    string m_name;
    byte m_key;
    int m_frameTime;
    float m_rotation;
    sf::Color m_color;
    sf::Vector2i m_position;
    sf::Vector2i m_offset;
    list<ModifierData*> m_modifiers;

public:
    void setName(const string& name) { m_name = name; }
    const string& getName() { return m_name; }

    void setKey(byte ft) { m_key = ft; }
    byte getKey() { return m_key;}

    void setColor(sf::Color& color) { m_color = color; }
    sf::Color getColor() { return m_color; }

    void setPosition(const sf::Vector2i& v) { m_position = v; }
    sf::Vector2i getPosition() { return m_position; }

    void setOffset(const sf::Vector2i& v) { m_offset = v; }
    sf::Vector2i getOffset() { return m_offset; }

    void setRotation(float ft) { m_rotation = ft; }
    float getRotation() { return m_rotation; }

    void setFrameTime(int ft) { m_frameTime = ft; }
    int getFrameTime() { return m_frameTime; }

    void addModifier(ModifierData* mod) { m_modifiers.push_back(mod); }
    void delModifier(ModifierData* mod) { m_modifiers.remove(mod); }

public:
    virtual bool save(libconfig::Setting& setting, string& error);
    virtual bool load(libconfig::Setting& setting, string& error);

    AnimationData();
    ~AnimationData();
};
typedef list<AnimationData*> AnimationDataList;

bool loadConfig(SceneData* scene, list<AnimationData*>& anims, const string& file, string& error);
bool saveConfig(SceneData* scene, list<AnimationData*>& anims, const string& file, string& error);



}
